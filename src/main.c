#include "mem_internals.h"
#include "mem.h"
#include "util.h"
static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst,struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static struct block_header *get_block_header (void *data){
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void first_test(){
    printf("1) Test: ");
    struct block_header *first_block = (struct block_header *) heap_init(8192);
    void *block = _malloc(200);
    if (first_block->capacity.bytes != 200) err("Error! Block has incorrect size\n");
    _free(block);
    printf("Success!\n");
}

void second_test(){
    printf("2) Test: ");
    void *block1 = _malloc(550);
    void *block2 = _malloc(550);
    if (block1 == NULL || block2 == NULL) err("Error! Block is null\n");
    struct block_header *first_block_data = get_block_header(block1);
    _free(block1);
    struct block_header *second_block_data = get_block_header(block2);
    if (first_block_data->is_free == false || second_block_data->is_free == true) err("Error! Incorrect answer\n");
    _free(block2);
    printf("Success!\n");
}

void third_test(){
    printf("3) Test: ");
    void *block1 = _malloc(670);
    void *block2 = _malloc(670);
    void *block3 = _malloc(670);
    if (block1 == NULL || block2 == NULL || block3 == NULL) err("Error! Block is null\n");
    _free(block1);
    struct block_header *block1_header = get_block_header(block1);
    _free(block2);
    struct block_header *block2_header = get_block_header(block2);
    struct block_header *block3_header = get_block_header(block3);
    if (block1_header->is_free == false || block2_header->is_free == false || block3_header->is_free == true) err("Error! Incorrect answer\n");
    _free(block3);
    printf("Success!\n");
}

void fourth_test(){
    printf("4) Test: ");
    void *block1 = _malloc(1000);
    void *block2 = _malloc(1000);
    void *block3 = _malloc(1000);
    if (block1 == NULL || block2 == NULL || block3 == NULL) err("Error! Block is null\n");
    struct block_header *block1_header = get_block_header(block1);
    struct block_header *block2_header = get_block_header(block2);
    if (!blocks_continuous(block1_header, block2_header)) err("Error! Blocks do not located in a row");
    _free(block1);
    _free(block2);
    _free(block3);
    printf("Success!\n");
}

int main(){
    first_test();
    second_test();
    third_test();
    fourth_test();
}
